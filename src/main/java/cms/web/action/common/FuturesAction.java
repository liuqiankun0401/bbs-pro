package cms.web.action.common;

import cms.bean.PageForm;
import cms.bean.user.User;
import cms.service.template.TemplateService;
import cms.service.user.UserService;
import cms.utils.JsonUtils;
import cms.utils.WebUtil;
import cms.web.action.AccessSourceDeviceManage;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
public class FuturesAction {
    @Resource
    TemplateService templateService;
    @Resource
    AccessSourceDeviceManage accessSourceDeviceManage;
    @Resource
    UserService userService;

    /**
     *
     * @param response
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/getFuturesList",method= RequestMethod.GET)
    public String getFuturesList(ModelMap model, PageForm pageForm,
                                    HttpServletRequest request, HttpServletResponse response)
            throws Exception {

        boolean isAjax = WebUtil.submitDataMode(request);//是否以Ajax方式提交数据

        List<String> list = new ArrayList<>();
        list.add("ddd");
        list.add("dbbb");
        model.addAttribute("list", list);

        WebUtil.writeToWeb(JsonUtils.toJSONString(list), "json", response);
        return null;
    }

    @ResponseBody
    @RequestMapping(value="/getUserInfo",method= RequestMethod.GET)
    public String getUserInfo (ModelMap model, PageForm pageForm,
                               HttpServletRequest request, HttpServletResponse response)
            throws IOException {
        String account = request.getParameter("account");
        User user = userService.findUserByAccount(account);
        Map<String, Object> map = new HashMap<>();
        map.put("success", true);

        String dirName = templateService.findTemplateDir_cache();
        String baseWechatDir = "common/" + dirName + "/";
        if (null != user) {
            map.put("mobile", user.getMobile());
            map.put("wechat", baseWechatDir+user.getRemarks());
            WebUtil.writeToWeb(JsonUtils.toJSONString(map), "json", response);
        } else {
            map.put("success", false);
            WebUtil.writeToWeb(JsonUtils.toJSONString(map), "json", response);
        }
        return null;
    }
}
